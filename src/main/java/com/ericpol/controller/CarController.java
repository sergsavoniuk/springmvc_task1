package com.ericpol.controller;

import com.ericpol.model.Car;
import com.ericpol.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.UUID;

@Controller
public class CarController {

    @Autowired
    private CarService carService;

    @RequestMapping(value = "/input-car", method = RequestMethod.GET)
    public String car(Model model) {
        Car car = new Car();
        car.setId(UUID.randomUUID());
        model.addAttribute("car", car);
        return "car";
    }

    @RequestMapping(value = "/save-car", method = RequestMethod.POST)
    public String saveCar(@ModelAttribute("car") Car car) {
        carService.saveCar(car);
        return "redirect:/list-cars";
    }

/*    @RequestMapping(value = "/save-car", method = RequestMethod.POST)
    public String saveCar(HttpServletRequest request) {
        Car newCar = new Car(request.getParameter("makes"), request.getParameter("model"),
                            request.getParameter("fuel"), Integer.valueOf(request.getParameter("year")),
                            Long.valueOf(request.getParameter("price")));
        carService.saveCar(newCar);
        return "redirect:/list-cars";
    }*/

    @RequestMapping(value = "/edit-car", method = RequestMethod.GET)
    public String editCar(@RequestParam("editCarId") UUID id, Model model) {
        Car editCar = carService.getCar(id);
        model.addAttribute("car", editCar);
        return "editCar";
    }

/*    @RequestMapping(value = "/edit-car", method = RequestMethod.POST)
    public ModelAndView editCar(HttpServletRequest request) {
        Car editCar = carService.getCar(Long.valueOf(request.getParameter("editCarId")));
        ModelAndView modelAndView = new ModelAndView("editCar");
        modelAndView.addObject("car", editCar);
        return modelAndView;
    }*/

    @RequestMapping(value = "/update-car", method = RequestMethod.POST)
    public String updateCar(@ModelAttribute("car") Car car) {
        carService.updateCar(car);
        return "redirect:/list-cars";
    }

    @RequestMapping(value = "/delete-car", method = RequestMethod.POST)
    public String deleteCar(@RequestParam("deleteCarId") UUID id) {
        carService.deleteCar(id);
        return "redirect:/list-cars";
    }

    @RequestMapping(value = "/list-cars", method = RequestMethod.GET)
    public String listOfCars(ModelMap model) {
        List<Car> cars = carService.getAllCars();
        model.put("cars", cars);
        return "carsList";
    }
}