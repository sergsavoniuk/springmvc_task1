package com.ericpol.service;

import com.ericpol.model.Car;

import java.util.List;
import java.util.UUID;

public interface CarService {
    Car saveCar(Car car);
    boolean deleteCar(UUID id);
    Car updateCar(Car car);
    Car getCar(UUID id);
    List<Car> getAllCars();
}
