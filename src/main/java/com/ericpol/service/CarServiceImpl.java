package com.ericpol.service;

import com.ericpol.model.Car;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Service
public class CarServiceImpl implements CarService {

    private List<Car> cars;

    @PostConstruct
    public void init() {
        cars = new ArrayList<>();
    }

    public Car saveCar(Car car) {
        cars.add(car);
        return car;
    }

    @Override
    public boolean deleteCar(UUID id) {
        for (Car car : cars) {
            if (car.getId().equals(id.toString())) {
                cars.remove(car);
                return true;
            }
        }
        return false;
    }

    @Override
    public Car updateCar(Car editCar) {
        for (int i = 0; i < cars.size(); ++i) {
            if (cars.get(i).getId().equals(editCar.getId())) {
                cars.set(i, editCar);
            }
        }
        return editCar;
    }

    @Override
    public Car getCar(UUID id) {
        for (Car car : cars) {
            if (car.getId().equals(id.toString())) {
                return car;
            }
        }
        return null;
    }

    @Override
    public List<Car> getAllCars() {
        return cars;
    }
}