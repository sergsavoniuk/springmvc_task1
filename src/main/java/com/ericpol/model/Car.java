package com.ericpol.model;

import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class Car {

    private UUID id;
    private String makes;
    private String model;
    private String fuel;
    private int year;
    private double price;

    public Car() {
    }

    public Car(UUID id, String makes, String model, String fuel, int year, double price) {
        this.id = id;
        this.makes = makes;
        this.model = model;
        this.fuel = fuel;
        this.year = year;
        this.price = price;
    }

    public String getId() {
        return id.toString();
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMakes() {
        return makes;
    }

    public void setMakes(String makes) {
        this.makes = makes;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}