<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>

<html lang="en">
<head>
    <title>Car List</title>
    <style type="text/css">
        TABLE {
            width: 300px;
            border: 2px solid black;
            background: silver;
        }
        TD, TH {
            text-align: center;
            padding: 3px;
            border: 1px solid black;
        }
        TH {
            background: #4682b4;
            color: white;
            border-bottom: 2px solid black;
        }
    </style>
</head>
<body>

<div>
    <h2>List of cars</h2>
    <a href="<c:url value="/input-car"/>">Add new car</a>
    <table>
        <thead>
        <th>Id</th>
        <th>Makes</th>
        <th>Model</th>
        <th>Fuel</th>
        <th>Year</th>
        <th>Price</th>
        <th></th>
        <th></th>
        </thead>
        <tbody>

        <c:forEach items="${cars}" var="car">
            <tr>
                <td><c:out value="${car.id}"/></td>
                <td><c:out value="${car.makes}"/></td>
                <td><c:out value="${car.model}"/></td>
                <td><c:out value="${car.fuel}"/></td>
                <td><c:out value="${car.year}"/></td>
                <td><c:out value="${car.price}"/></td>


                <td>
                    <form id="edit" method="GET" action="/edit-car">
                        <input type="hidden" name = "editCarId" value = "${car.id}"/>
                        <input type="submit" value="Edit"/>
                    </form>
                </td>



                <td>
                    <form id="delete" method="POST" action="/delete-car">
                        <input type="hidden" name = "deleteCarId" value = "${car.id}"/>
                        <input type="submit" value="Delete"/>
                    </form>
                </td>

            </tr>

        </c:forEach>
        </tbody>
    </table>
</div>

</body>

</html>