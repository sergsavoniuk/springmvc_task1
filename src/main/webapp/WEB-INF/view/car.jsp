<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Add car</title>
    <style type="text/css">
        .modal{
            padding: 50px;
            position: fixed; top: 15%; left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }
    </style>

</head>
<body>
<form:form action="/save-car" method="POST" commandName="car">
    <div class="modal">
        <h2 align="center">Add car</h2>
        <table cellspacing="0" cellpadding="5">
            <tr>
                <td>Id</td>
                <td><input type="text" readonly="readonly" size="20" name="id" value=${car.id}></td>
            </tr>
            <tr>
                <td>Makes</td>
                <td><input type="text" size="20" name="makes"></td>
            </tr>
            <tr>
                <td>Model</td>
                <td><input type="text" size="20" name="model"></td>
            </tr>
            <tr>
                <td>Fuel</td>
                <td><input type="text" size="20" name="fuel"></td>
            </tr>
            <tr>
                <td>Year</td>
                <td><input type="text" size="20" name="year"></td>
            </tr>
            <tr>
                <td>Price</td>
                <td><input type="text" size="20" name="price"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Add car"></td>
            </tr>
        </table>
    </div>

</form:form>
</body>
</html>