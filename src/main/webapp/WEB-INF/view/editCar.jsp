<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>

<html lang="en">
<head>
    <title>Edit car</title>
    <style type="text/css">
        .modal{
            padding: 50px;
            position: fixed; top: 13%; left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
        }
    </style>

</head>
<body>
<form:form action="/update-car" method="POST" commandName="car">
    <div class="modal">
        <h2 align="center">Add car</h2>
        <table cellspacing="0" cellpadding="5">
            <tr>
                <td>Id</td>
                <td><input type="text" readonly="readonly" name="id" value=${car.id}></td>
            </tr>
            <tr>
                <td>Makes</td>
                <td><input type="text" size="20" name="makes" value=${car.makes}></td>
            </tr>
            <tr>
                <td>Model</td>
                <td><input type="text" size="20" name="model" value=${car.model}></td>
            </tr>
            <tr>
                <td>Fuel</td>
                <td><input type="text" size="20" name="fuel" value=${car.fuel}></td>
            </tr>
            <tr>
                <td>Year</td>
                <td><input type="text" size="20" name="year" value=${car.year}></td>
            </tr>
            <tr>
                <td>Price</td>
                <td><input type="text" size="20" name="price" value=${car.price}></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Update"></td>
            </tr>
        </table>
    </div>

</form:form>
</body>
</html>
